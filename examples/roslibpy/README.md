# Roslibpy - connect to ROS with pure python

Python ROS Bridge library allows to use Python and IronPython to interact with ROS, the open-source robotic middleware. It uses WebSockets to connect to rosbridge 2.0 and provides publishing, subscribing, service calls, actionlib, TF, and other essential ROS functionality.

Unlike the rospy library, this does not require a local ROS environment, allowing usage from platforms other than Linux.


see also:

* [github](https://github.com/gramaziokohler/roslibpy)
* [documentation](https://roslibpy.readthedocs.io/en/latest/)
