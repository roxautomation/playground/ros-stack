# /usr/bin/env python
import time

import roslibpy  # type: ignore

client = roslibpy.Ros(host="rosbridge", port=9090)
client.run()

chatter_topic = roslibpy.Topic(client, "/chatter", "std_msgs/String")
cmd_vel_topic = roslibpy.Topic(client, "/cmd_vel", "geometry_msgs/Twist")


idx = 0
try:
    while client.is_connected:
        # --- chatter
        msg = f"roslib talker msg {idx}"
        print(f">>{msg}")
        chatter_topic.publish(roslibpy.Message({"data": msg}))

        # --- cmd_vel
        vel_msg = {
            "linear": {"x": 0.1, "y": 0.2, "z": 0.0},
            "angular": {"x": 0.0, "y": 0.0, "z": 0.0},
        }
        print(f">>{vel_msg}")
        cmd_vel_topic.publish(roslibpy.Message(vel_msg))
        time.sleep(1)
        idx += 1
except KeyboardInterrupt:
    print("Keyboard interrupt received. Exiting.")
    chatter_topic.unadvertise()
    client.terminate()
