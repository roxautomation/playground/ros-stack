#!/usr/bin/env python3
"""
 Demonstates how to connect to ROS using rosbridge

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""
import roslibpy  # type: ignore

TOPIC = "/chatter"
print(f"Connecting to {TOPIC}")

client = roslibpy.Ros(host="rosbridge", port=9090)


listener = roslibpy.Topic(client, TOPIC, "std_msgs/String")
listener.subscribe(lambda message: print(f"Received: {message['data']}"))

try:
    client.run_forever()
except KeyboardInterrupt:
    client.terminate()
