# ROS2 playground

This repo demonstrates how to to run ROS2 in Docker and access is through [rosbridge](http://wiki.ros.org/rosbridge_suite)


## What is it

A development container stack containing:

* `talker` is running `talker_node.py`, posting:
    - text message on `/chatter` every  5 seconds
    - sine wave on `/signals/sine`
    - ramp signal on `/signals/ramp`

* [`rosboard`](https://github.com/dheera/rosboard)  is running web ui on [localhost:8888](http://localhost:8888)
* `rosbridge` provides a way for non-ros code to interact with the stack through a websocket
running [rosbridge protocol](https://github.com/biobotus/rosbridge_suite/blob/master/ROSBRIDGE_PROTOCOL.md)
* `devcontainer` is a ROS2 container that can be used for development.

## Running stack

Main starting point is `.docker-compose.yml`. Take a look at defined containers.

### In VSCode

Everything is started at once when this folder is opened with configuration defined in `.devcontainer/devcontainer.json`.

VSCode then automatically connects to `devcontainer`.


### Standalone

1. start docker stack with `docker-compose up`
2. connect to devcontainer with `docker exec -it devcontainer bash`. This will take you to
devcontainer shell.

## Example code

is located in `examples` folder. In `devcontainer` current directory is mounted in `/workspace`. So `examples` folder can be found at `/workspace/examples`

* run `listen.py` to listen to `chatter` topic. It uses [roslibpy](https://roslibpy.readthedocs.io/en/latest/) to connect to ros stack through a websocket.

## See also

* copy of a post about this repo in `misc/medium_post.md`
