#!/usr/bin/env python3
"""
 Simple talker demo that publishes data on several topics


 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import math
import random
import string
import time

import rclpy  # type: ignore
from rclpy.node import Node  # type: ignore
from std_msgs.msg import Float32, String  #  type: ignore


# topic definitions
CHATTER_TOPIC = "chatter"
SINE_TOPIC = "/signals/sine"
RAMP_TOPIC = "/signals/ramp"

# other constants
SINE_FREQUENCY = 1  # Hz
CHATTER_FREQUENCY = 0.2  # Hz


def generate_random_hash(length=4):
    return "".join(random.choices(string.ascii_lowercase + string.digits, k=length))


class TalkerNode(Node):
    def __init__(self, name=None):
        if name is None:
            name = "talker_" + generate_random_hash()
        super().__init__(name)
        self.string_publisher = self.create_publisher(String, CHATTER_TOPIC, 10)
        self.sine_publisher = self.create_publisher(Float32, SINE_TOPIC, 10)
        self.ramp_publisher = self.create_publisher(Float32, RAMP_TOPIC, 10)

        # Timer for string messages
        self.string_timer = self.create_timer(
            1 / CHATTER_FREQUENCY, self.string_timer_callback
        )
        self.counter = 0

        # Timer for sine wave signal
        sine_timer_period = 0.1  # seconds
        self.sine_timer = self.create_timer(
            sine_timer_period, self.signal_timer_callback
        )
        self._start_time = time.time()
        self._ramp_val = 0.0

    def string_timer_callback(self):
        msg = String()
        msg.data = f"Hello from {self.get_name()} {self.counter}"
        self.string_publisher.publish(msg)
        self.get_logger().info(f"Publishing: '{msg.data}'")
        self.counter += 1

    def signal_timer_callback(self):
        # generate signals
        self.generate_sine_wave()
        self.generate_ramp()

    def generate_sine_wave(self):
        sine_msg = Float32()

        t_elapsed = time.time() - self._start_time

        sine_msg.data = math.sin(
            2 * math.pi * SINE_FREQUENCY * t_elapsed
        )  # 5 Hz sine wave
        self.sine_publisher.publish(sine_msg)
        self.get_logger().info(f"Publishing sine wave: '{sine_msg.data}'")

    def generate_ramp(self, step=10):
        self._ramp_val += step
        if self._ramp_val > 1000:
            self._ramp_val = 0.0

        ramp_msg = Float32()

        ramp_msg.data = self._ramp_val
        self.ramp_publisher.publish(ramp_msg)
        self.get_logger().info(f"Publishing ramp: '{ramp_msg.data}'")


def main():
    rclpy.init()
    try:
        node = TalkerNode()
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
